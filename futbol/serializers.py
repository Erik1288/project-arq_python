from futbol.models import PositionsTable
from rest_framework import serializers


'''defining serializers, to use them in the representation of our data'''


class PositionsTableSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PositionsTable
        fields = ['name', 'positions']
