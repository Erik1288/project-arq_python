from __future__ import absolute_import
import requests
from bs4 import BeautifulSoup
from futbol.models import PositionsTable
from futbol_colombia.celery import app


'''function in charge of scraping in the web portal as.com, 
   to the Colombian soccer teams, and each of their positions.'''


@app.task
def scrapingFutbol():

    page = requests.get(
        # as.com leaderboard links
        'https://colombia.as.com/resultados/futbol/colombia_i/clasificacion/')

    soup = BeautifulSoup(page.text, 'html.parser')

    page_html = soup.find_all('th', scope="row")

    teamList = []
    count = 0
    for item in page_html:
        if count < 20:
            name = item.find(class_="nombre-equipo").text
            positions = item.find(class_="pos").text
            dic = {'name': name, 'positions': positions}
            teamList.append(dic)
        else:
            break
        count += 1

    for items in teamList:
        PositionsTable.objects.update_or_create(**items)
    return PositionsTable.objects.all()
