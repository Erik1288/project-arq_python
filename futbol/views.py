from futbol.models import PositionsTable
from rest_framework import viewsets
from futbol.serializers import PositionsTableSerializer


'''class-view that allows to list all the equipment and serialize them'''


class PositionsTableViewSet(viewsets.ModelViewSet):
    queryset = PositionsTable.objects.all()
    serializer_class = PositionsTableSerializer
