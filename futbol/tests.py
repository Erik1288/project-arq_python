from django.test import TestCase
from .tasks import scrapingFutbol
import json


class UserTestCase(TestCase):

    '''try function scraping, methods: get, put, delete, get(empty database)'''

    def setUp(self):
        self.url = 'http://127.0.0.1:8000/positions/'
        self.element = {
            "name": "Tolima",
            "positions": "1"
        }

    def test_scraping_futbol_colombia(self):
        resp = scrapingFutbol()
        self.assertIsNotNone(resp)

    def test_post_futbol(self):
        resp = self.client.post(self.url, self.element)
        self.assertEqual(resp.status_code, 201)
        result = json.loads(resp.content)
        self.assertEqual(result.get('name'), self.element.get('name'))

    def test_get_futbol(self):
        self.client.post(self.url, self.element)
        resp = self.client.get(self.url)
        self.assertEqual(resp.status_code, 200)
        result = json.loads(resp.content)
        self.assertEqual(len(result.get('results')), 1)

    def test_get_futbol_empty(self):
        resp = self.client.get(self.url)
        self.assertEqual(resp.status_code, 200)
        result = json.loads(resp.content)
        self.assertEqual(len(result.get('results')), 0)

    def test_delete_futbol(self):
        self.client.post(self.url, self.element)
        url_element = 'http://127.0.0.1:8000/positions/1/'
        resp = self.client.delete(url_element)
        self.assertEqual(resp.status_code, 204)

    def test_put_futbol(self):
        element_update = {
            "name": "Tolima Pijaos",
            "positions": "1"
        }

        self.client.post(self.url, self.element)
        url_element = 'http://127.0.0.1:8000/positions/1/'
        resp = self.client.put(
            url_element, element_update, content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        result = json.loads(resp.content)
        self.assertEqual(result.get('name'), element_update.get('name'))
