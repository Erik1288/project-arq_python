# module that defines the celery instance
from __future__ import absolute_import
import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'futbol_colombia.settings')

app = Celery('futbol_colombia')

app.config_from_object('django.conf.settings', namespace='CELERY')
app.autodiscover_tasks()
