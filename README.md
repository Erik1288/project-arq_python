## instalacion proyecto

1. crear y activar un entorno virtual
2. `pip install -r requirements.txt`
3. `python3 manage.py migrate`

## corriendo proyecto

1. `python3 manage.py runserver`
2. ` sudo docker run -p 5672:5672 --name futbol_colombia rabbitmq:3`
3. `celery -A futbol_colombia worker -l info`
4. `celery -A futbol_colombia beat -l info`

## realizando testing

1. `coverage erase`
2. `coverage run manage.py test`
3. `coverage report`
4. `coverage html`
